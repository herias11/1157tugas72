<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Hapus</title>
    </head>
    <body>
        <h1>Hapus</h1>
        <form action="/hapus" method="post">
            <input type="hidden" name="hdnId" value="${data.key.id}">
            Nama: <input type="text" name="txtNama" value="${data.properties.nama}" disabled><br/>
            NIM: <input type="number" name="txtNIM" value="${data.properties.nim}" disabled><br/>
            Email: <input type="text" name="txtEmail" value="${data.properties.email}" disabled><br/>
            NoHP: <input type="number" name="txtNoHP" value="${data.properties.noHP}" disabled><br/>
            Aktif: <input type="text" name="txtAktif" value="${data.properties.aktif}" disabled><br/>
            <input type="submit" value="Hapus">
        </form>
    </body>
</html>


