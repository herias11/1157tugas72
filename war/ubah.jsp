<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Ubah</title>
    </head>
    <body>
        <h1>Ubah</h1>
        <form action="/ubah" method="post">
            <input type="hidden" name="hdnId" value="${data.key.id}">
            Nama: <input type="text" name="txtNama" value="${data.properties.nama}"><br/>
            NIM: <input type="number" name="txtNIM" value="${data.properties.nim}"><br/>
            Email: <input type="text" name="txtEmail" value="${data.properties.email}"><br/>
            NoHP: <input type="number" name="txtNoHP" value="${data.properties.noHP}"><br/>
            Aktif: <input type="text" name="txtAktif" value="${data.properties.aktif}"><br/>
            <input type="submit" value="ubah">
        </form>
    </body>
</html>

